{ config, pkgs, lib, ... }:

# UwU Style for NixOS

let
  nixuwu = pkgs.fetchFromGitHub {
    owner = "TilCreator";
    repo = "NixOwO";
    rev = "d2bc0591d78f95d1659da9d9f5e2f07615075fd7";
    sha256 = "sha256-5H9PRSY77oMABvgNDZ/wBayEBXJTzE9sGeAjkFAqYIQ=";
  };

  nixuwu-icon = pkgs.runCommandLocal "nixuwu-icon" {  }
  ''
    install -m644 ${nixuwu}/NixOwO_plain.svg -D $out/share/icons/hicolor/scalable/apps/nix-snowflake.svg
  '';
  meta.priority = 10;

  nixuwu-boot = pkgs.runCommandLocal "nixuwu-boot"
  { nativeBuildInputs = [ pkgs.imagemagick ]; }
  ''
    mkdir $out
    # convert logo to png
    convert -background none ${nixuwu}/NixOwO_plain.svg logo.png
    # resize logo
    convert logo.png -resize 256x256 $out/logo.png
  '';
in
{
  boot.plymouth.enable = true;
  boot.initrd.systemd.enable = true;
  boot.plymouth.logo = "${nixuwu-boot}/logo.png";
  boot.plymouth.theme = "breeze"; # default is bgrt
  #boot.plymouth.themePackages = "";

  environment.systemPackages = [
    nixuwu-icon
  ];
}