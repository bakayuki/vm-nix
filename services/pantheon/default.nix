{ config, pkgs, lib, ... }:

#
# Enable the Pantheon Desktop
#

{
  services = {
    xserver = {
      enable = true;
      layout = "us";
      xkbVariant = "";

      desktopManager.pantheon.enable = true;

      displayManager = {
        autoLogin = {
          enable = true;
          user = "yuki";
        };
        lightdm.enable = true;
      };
    };
  };
}