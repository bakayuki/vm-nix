{
    description = "Experimental NixOS Configuration Flake";

    inputs = {
        sops-nix = {
            url = "github:Mic92/sops-nix";
            inputs.nixpkgs.follows = "nixpkgs";
        };

        nixpkgs.url  = "github:nixos/nixpkgs/nixos-22.11";
        unstable.url = "github:nixos/nixpkgs/nixos-unstable";
    };

    outputs = { self, nixpkgs, unstable, sops-nix }@attrs:
        let
            pkgs = nixpkgs;
            upkgs = unstable;
        in {
            nixosConfigurations = {
                nixos = nixpkgs.lib.nixosSystem {
                    system = "x86_64-linux";
                    specialArgs = attrs;
                    modules = [
                        ./hosts/nixos
                    ];
                };
            };
        };
}