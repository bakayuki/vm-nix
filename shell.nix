#
# shell.nix
# This creates an environment for working with SOPS keys.
# Reduces the complexity of deploying SOPS on a new system.
#

with import <nixpkgs> {};
let
  sops-nix = builtins.fetchTarball {
    url = "https://github.com/Mic92/sops-nix/archive/master.tar.gz";
  };
in
mkShell {
  nativeBuildInputs = [
    age
    sops
    sops-nix
  ];
}