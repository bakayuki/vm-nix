{ sops-nix, config, ... }:

#
# This file imports secrets for the host.
# All SOPS configuration for a host should be done here,
# the keys should then be used throughout the configuration.
#
{
  imports = [ sops-nix.nixosModules.sops ];
  sops.defaultSopsFile = ../../secrets/nixos.yaml;
  sops.age.sshKeyPaths = [ "/etc/ssh/ssh_host_ed25519_key" ];
  sops.secrets.test-passwordFile.neededForUsers = true;
  sops.secrets.test-passwordFile = {};
  sops.secrets."nextcloud/admin-passFile" = {};
}