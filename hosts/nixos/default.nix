{ config, pkgs, ... }:

{
  imports =
    [ 
      ../../hardware/nixos
      ../../services/pantheon
      ../../sops/nixos
      ../../containers/postgresql
      ../../containers/nextcloud
      ../../containers/nginx
    ];

  #
  # Boot Configuration
  #
  boot = {
    loader = {
      efi = {
        canTouchEfiVariables = true;
        efiSysMountPoint = "/boot/efi";
      };
      systemd-boot.enable = true;
    };
  };

  #
  # Networking Configuration
  #
  networking = {
    hostName = "nixos";
    networkmanager.enable = true;

    firewall = {
      enable = true;
      allowedTCPPorts = [ 80 ];
    };

    # container stuff
    nat = {
      enable = true;
      internalInterfaces = [
        "ve-postgres"
        "ve-nextcloud"
      ];
      externalInterface = "enp0s3";
    };
  };


  #
  # Timezone and Localization
  #
  time.timeZone = "Europe/Berlin";
  i18n.defaultLocale = "en_US.UTF-8";

  #
  # User Configuration
  #
  users.users.yuki = {
    isNormalUser = true;
    description = "Yuki";
    extraGroups = [ "networkmanager" "wheel" "keys" ];
  };
  users.users.test = {
    isNormalUser = true;
    description = "Test User";
    passwordFile = config.sops.secrets.test-passwordFile.path;
  };

  #
  # Services Configuration
  #
  services.openssh.enable = true;
  services.openssh.passwordAuthentication = true;

  # Nix & nixpkgs Configuration
  nix.settings.experimental-features = [ "nix-command" "flakes" ];
  nixpkgs.config.allowUnfree = true;

  #
  # Installed Packages
  #
  environment.systemPackages = with pkgs; [
    git
  ];

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "22.11"; # Did you read the comment?

}
