{ config, pkgs, ... }:

#
# Create a container for Nextcloud
# TODO: see if there is a way to thin this out.
# right now this container contains an nginx instance and
# all we're doing is pointing to it from our main nginx instance.
# it would be nice to cut out the middleman.
#
{
  containers.nextcloud = {
    # all stateful data should be explicit.
    # destroy container on shutdown.
    ephemeral = true;
    autoStart = true;

    # host storage mapping
    bindMounts = {
      "/var/lib/nextcloud" = {
        hostPath = "/data/nextcloud";
        isReadOnly = false;
      };
      "/run/secrets/nextcloud" = {
        hostPath = config.sops.secrets."nextcloud/admin-passFile".path;
        isReadOnly = true;
      };
    };

    #
    # Networking Block
    #
    privateNetwork = true;
    hostAddress = "172.16.0.2";
    localAddress = "172.16.0.20";

    config = {
      # ensure postgres can write to the data directory
      systemd.tmpfiles.rules = [
        "d /var/lib/nextcloud 700 nextcloud nextcloud -"
        "C /tmp/authfile - - - - /run/secrets/nextcloud"
        "z /tmp/authfile 700 nextcloud nextcloud -"
      ];

      #
      # Firewall Block
      #
      networking.firewall = {
        enable = true;
        allowedTCPPorts = [ 80 ];
      };

      #
      # Nextcloud Instance Block
      #
      services.nextcloud = {
        enable = true;
        hostName = "nextcloud.test.com";
        package = pkgs.nextcloud25;
        config = {
          dbtype = "pgsql";
          dbuser = "nextcloud";
          dbhost = config.containers.postgresql.localAddress;
          dbport = config.containers.postgresql.config.services.postgresql.port;
          dbname = "nextcloud";
          adminuser = "root";
          adminpassFile = "/tmp/authfile";
          extraTrustedDomains = [ "*.*.*.*" ];
        };
      };

      # nix declaration
      system.stateVersion = "22.11";
    };
  };
}