{ config, pkgs, ... }:

#
# Create a container for Postgresql
#
{
  containers.postgresql = {
    # all stateful data should be explicit.
    # destroy container on shutdown.
    ephemeral = true;
    autoStart = true;

    # host storage mapping
    bindMounts = {
      "/var/lib/postgresql" = {
        hostPath = "/data/postgresql";
        isReadOnly = false;
      };
    };

    #
    # Networking Block
    #
    privateNetwork = true;
    hostAddress = "172.16.0.2";
    localAddress = "172.16.0.10";

    config = {
      # ensure postgres can write to the data directory
      systemd.tmpfiles.rules = [
        "d /var/lib/postgresql 700 postgres postgres -"
      ];

      # firewall
      # this will make things break if they're not configured properly
      # frustrating, but useful.
      networking.firewall = {
        enable = true;
        allowedTCPPorts = [ 5432 ];
      };

      services.postgresql = {
        # we need to explicitly declare our postgresql version.
        # each major upgrade requires imperative operations to be ran,
        # and we don't want to be caught out.
        package = pkgs.postgresql_14;
        enable = true;
        # containerized pgsql will need network access
        enableTCPIP = true;
        port = 5432;

        ensureDatabases = [ "nextcloud" ];
        ensureUsers = [
          {
            name = "nextcloud";
            ensurePermissions."DATABASE nextcloud" = "ALL PRIVILEGES";
          }
        ];

        # this is hacky but it does work. at some point investigate a better way to do this.
        # TODO: it seems possible to use ldap here. this would remove secrets management too.
        authentication = ''
        # Append options for container configuration
        # TYPE  DATABASE    USER        ADDRESS           METHOD
        host    nextcloud   nextcloud   172.16.0.20/32    trust
        '';

      };

      # nix declaration
      system.stateVersion = "22.11";
    };
  };
}