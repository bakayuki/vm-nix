{ config, pkgs, ... }:

#
# Create a container for NGINX
#
{
  containers.nginx = {
    # all stateful data should be explicit.
    # destroy container on shutdown.
    ephemeral = true;
    autoStart = true;

    #
    # Networking Block
    #
    privateNetwork = true;
    hostAddress = "172.16.0.2";
    localAddress = "172.16.0.30";
    forwardPorts = [
      {
        containerPort = 80;
        hostPort = 80;
        protocol = "tcp";
      }
      {
        containerPort = 443;
        hostPort = 443;
        protocol = "tcp";
      }
    ];

    config = {
      #
      # Firewall Block
      #
      networking.firewall = {
        enable = true;
        allowedTCPPorts = [ 80 443 ];
      };

      services.nginx = {
        enable = true;
        recommendedProxySettings = true;
        recommendedTlsSettings = true;
        virtualHosts = {
          "192.168.2.150" = {
            locations."/".proxyPass = "http://172.16.0.20:80";
          };
        };
      };

      # nix declaration
      system.stateVersion = "22.11";
    };
  };
}